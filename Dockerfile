FROM node:8.7
RUN mkdir /app
WORKDIR /app
RUN apt-get install -y libpng12-0 python
RUN wget https://github.com/yarnpkg/yarn/releases/download/v0.20.0/yarn_0.20.0_all.deb
RUN dpkg -i yarn_0.20.0_all.deb
# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
ADD package.json /tmp/package.json
ADD yarn.lock /tmp/yarn.lock
RUN cd /tmp && yarn install
RUN mkdir -p /app && cp -a /tmp/node_modules /app/
# From here we load our application's code in, therefore the previous docker
# "layer" thats been cached will be used if possible
ADD . /app
RUN NODE_ENV=production npm run build
EXPOSE 8000
CMD cd build && python -m SimpleHTTPServer
