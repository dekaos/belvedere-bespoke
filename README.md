# Hotsite - Belvedere Bespoke

Para o desenvolvimento deste projeto é necessário um ambiente com [nodejs](https://nodejs.org/en/) instalado, preferivelmente a versão 8.7.0. 
O versionamento do node pode ser conseguido instalando o [nvm](https://github.com/creationix/nvm), após instalado basta rodar o comando
 ```
 nvm use 
 ```
 dentro do diretório do projeto.


## Instalando as dependências

```sh
npm install
```

> Finalizada a instalação, podemos iniciar o desenvolvimento


## Fluxo de trabalho de desenvolvimento


**1. O servidor live-reload é iniciado com o comando:**

```sh
npm run dev
```

> Sempre que fizer alterações no diretório `src`, ele irá reconstruir e atualizar o navegador com as novas modificações.


**2. Gerando uma versão para produção:**

```sh
npm run build
```
O comando vai gerar o diretório `./build` que contém a versão de produção da aplicação.

**3. Simulando uma versão em produção localmente, [serve](https://github.com/zeit/serve):**

```sh
npm start
```


---


## Estrutura dos diretórios

```
|-- belevedere-bespoke
    |-- .babelrc
    |-- .eslintrc
    |-- .gitignore
    |-- .gitlab-ci.yml
    |-- .nvmrc
    |-- .travis.yml
    |-- Dockerfile
    |-- README.md
    |-- package.json
    |-- webpack.config.babel.js
    |-- yarn.lock
    |-- src
    |   |-- config.json
    |   |-- favicon.ico
    |   |-- index.ejs
    |   |-- index.js
    |   |-- manifest.json
    |   |-- pwa.js
    |   |-- assets
    |   |   |-- .gitkeep
    |   |   |-- fonts
    |   |   |   |-- Belvedere-Regular.woff
    |   |   |   |-- Belvedere-Regular.woff2
    |   |   |   |-- Knockout-HTF28-JuniorFeatherwt.woff
    |   |   |   |-- Knockout-HTF28-JuniorFeatherwt.woff2
    |   |   |   |-- TradeGothicLTStd-Bd2.woff
    |   |   |   |-- TradeGothicLTStd-Bd2.woff2
    |   |   |   |-- TradeGothicLTStd-BdCn20.woff
    |   |   |   |-- TradeGothicLTStd-BdCn20.woff2
    |   |   |   |-- TradeGothicLTStd.woff
    |   |   |   |-- TradeGothicLTStd.woff2
    |   |   |-- icons
    |   |   |   |-- android-chrome-192x192.png
    |   |   |   |-- android-chrome-512x512.png
    |   |   |   |-- apple-touch-icon.png
    |   |   |   |-- favicon-16x16.png
    |   |   |   |-- favicon-32x32.png
    |   |   |   |-- favicon.ico
    |   |   |   |-- mstile-150x150.png
    |   |   |-- images
    |   |   |   |-- arrow-down.svg
    |   |   |   |-- arrow-up.svg
    |   |   |   |-- back-blue.svg
    |   |   |   |-- back-white.svg
    |   |   |   |-- banner-1.jpg
    |   |   |   |-- be-inspired-1.png
    |   |   |   |-- be-inspired-2.png
    |   |   |   |-- be-inspired-3.png
    |   |   |   |-- be-inspired-large-1.jpg
    |   |   |   |-- be-inspired-large-2.jpg
    |   |   |   |-- be-inspired-large-3.jpg
    |   |   |   |-- be-inspired-video-cover.jpg
    |   |   |   |-- box.svg
    |   |   |   |-- calendar.svg
    |   |   |   |-- caret-up.svg
    |   |   |   |-- cart.svg
    |   |   |   |-- charge-2x.png
    |   |   |   |-- charge.png
    |   |   |   |-- close-blue.svg
    |   |   |   |-- close-white.svg
    |   |   |   |-- contact-mail.svg
    |   |   |   |-- contact-phone.svg
    |   |   |   |-- custom-bottle-2x.png
    |   |   |   |-- custom-bottle-lens-flare-2x.png
    |   |   |   |-- custom-bottle-lens-flare.png
    |   |   |   |-- custom-bottle-reflex-2x.png
    |   |   |   |-- custom-bottle-reflex.png
    |   |   |   |-- custom-bottle.png
    |   |   |   |-- facebook.svg
    |   |   |   |-- famous-drink.svg
    |   |   |   |-- info.svg
    |   |   |   |-- instagram.svg
    |   |   |   |-- left-arrow.svg
    |   |   |   |-- left-base-bottle-2x.png
    |   |   |   |-- left-base-bottle.png
    |   |   |   |-- left-led-bottle-2x.png
    |   |   |   |-- left-led-bottle.png
    |   |   |   |-- left-tree.svg
    |   |   |   |-- lens-flare-2x.png
    |   |   |   |-- lens-flare.png
    |   |   |   |-- linkedin.svg
    |   |   |   |-- logo-vodka.svg
    |   |   |   |-- mail.svg
    |   |   |   |-- minus.svg
    |   |   |   |-- modal-logo.svg
    |   |   |   |-- play.svg
    |   |   |   |-- plus.svg
    |   |   |   |-- product.jpg
    |   |   |   |-- result-bottle-2x.png
    |   |   |   |-- result-bottle.png
    |   |   |   |-- result-led-bottle-2x.png
    |   |   |   |-- result-led-bottle.png
    |   |   |   |-- result-lens-flare-2x.png
    |   |   |   |-- result-lens-flare.png
    |   |   |   |-- right-arrow.svg
    |   |   |   |-- right-base-bottle-2x.png
    |   |   |   |-- right-base-bottle.png
    |   |   |   |-- right-led-bottle-2x.png
    |   |   |   |-- right-led-bottle.png
    |   |   |   |-- right-tree.svg
    |   |   |-- video
    |   |       |-- belvedere-video-load-1.mp4
    |   |       |-- belvedere-video-load-1.webm
    |   |-- components
    |   |   |-- app.js
    |   |   |-- be-inspired
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |   |-- vendor
    |   |   |       |-- js
    |   |   |           |-- swiper.min.js
    |   |   |-- belvedere-bespoke
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- checkout
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- contact
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- customize
    |   |   |   |-- bad-words.js
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- days-of-operation-modal
    |   |   |   |-- index.js
    |   |   |   |-- style.scss   
    |   |   |-- footer
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- header
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- home
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |   |-- home-bottle
    |   |   |       |-- index.js
    |   |   |       |-- style.scss
    |   |   |-- my-products
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |   |-- cart
    |   |   |       |-- index.js
    |   |   |       |-- style.scss
    |   |   |-- page-modal
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- sanitize
    |   |   |   |-- index.js
    |   |   |-- sidebar
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- social-icons
    |   |   |   |-- index.js
    |   |   |   |-- style.scss
    |   |   |-- your-belvedere
    |   |       |-- index.js
    |   |       |-- style.scss
    |   |       |-- freight-modal
    |   |           |-- index.js
    |   |           |-- style.scss
    |   |-- lib
    |   |   |-- .gitkeep
    |   |-- style
    |       |-- fonts.scss
    |       |-- helpers.scss
    |       |-- index.scss
    |       |-- mixins.scss
    |       |-- variables.scss
    |       |-- vendor
    |           |-- animate.css
    |           |-- swiper.min.css
    |-- test
        |-- index.html
        |-- setup.js
        |-- components
            |-- app.test.js
            |-- header
            |   |-- index.test.js
            |-- home
                |-- index.test.js

```

O arquivo `app.js` localizado no diretório `src/components` é o ponto de entrada de todos os componentes da aplicação, 
é responsável também pelo gerenciamento das URL's.

---

## Estrutura das páginas por componente
#### Home
URL: /

Componente
```
|-- belevedere-bespoke
     |-- src
         |-- components
             |-- home
```

#### Customize e compre
URL: /customize-e-compre

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- customize
```

#### Inspire-se
URL: /inspire-se

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- be-inspired
```

#### Fale conosco
URL: /fale-conosco

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- contact
```

#### Sua Belvedere Bespoke
URL: /sua-belvedere

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- your-belvedere
```

#### Checkout
URL: /checkout

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- checkout
```

#### Meus pedidos
URL: /meus-pedidos

Componente
```
|-- belvedere-bespoke
    |-- src
        |-- components
            |-- my-products
```

## Estrutura das CSS
Os estilos podem ser adicionados/editados por componente ou globalmente, o diretório `style` dentro de `src` contém os arquivos
de estilo globais, o arquivo `index.scss` é responsável pelo carregamento desses estilos. Estilos globais não usam CSS Modules.

---
