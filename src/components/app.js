import { h, Component } from 'preact';
import { Router } from 'preact-router';

import Home from './home';
import Customize from './customize';
import BeInspired from './be-inspired';
import Contact from './contact';
import PageModal from './page-modal';
import YourBelvedere from './your-belvedere';
import Checkout from './checkout';
import customBottleImage from '../assets/images/custom-bottle.png';
import customBottleImage2x from '../assets/images/custom-bottle-2x.png';
import leftBaseBottle from '../assets/images/left-base-bottle.png';
import leftBaseBottle2x from '../assets/images/left-base-bottle-2x.png';
import resultBottle from '../assets/images/result-bottle.png';
import resultBottle2x from '../assets/images/result-bottle-2x.png';
import rightBaseBottle from '../assets/images/right-base-bottle.png';
import rightBaseBottle2x from '../assets/images/right-base-bottle-2x.png';
import customBottleLensFlare from '../assets/images/custom-bottle-lens-flare.png';
import customBottleLensFlare2x from '../assets/images/custom-bottle-lens-flare-2x.png';
import resultLensFlare from '../assets/images/result-lens-flare.png';
import resultLensFlare2x from '../assets/images/result-lens-flare-2x.png';
import backWhite from '../assets/images/back-white.svg';
import closekWhite from '../assets/images/close-white.svg';
import leftLedBottle from '../assets/images/left-led-bottle.png';
import leftLedBottle2x from '../assets/images/left-led-bottle-2x.png';
import MyProducts from './my-products';
import loadImage from 'image-promise';
import ReactGA from 'react-ga';

export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */

	constructor() {
		super();
		ReactGA.initialize('UA-110184213-1');
	}

	handleRoute = e => {
		this.currentUrl = e.url;
		const title = e.current.attributes.title || 'Belvedere Vodka';
		window.scrollTo(0, -100);
		ReactGA.ga('send', 'pageview', {'page': this.currentUrl, title});
	};

	componentDidMount() {
		const images = [
			customBottleImage,
			customBottleImage2x,
			leftBaseBottle,
			leftBaseBottle2x,
			resultBottle,
			resultBottle2x,
			rightBaseBottle,
			rightBaseBottle2x,
			customBottleLensFlare,
			customBottleLensFlare2x,
			resultLensFlare,
			resultLensFlare2x,
			backWhite,
			closekWhite,
			leftLedBottle,
			leftLedBottle2x
		];

		loadImage(images).then(() => {}).catch(() => {});
	}

	render() {
		return (
			<div id="app">
				<div class="left-tree" />
				<div class="right-tree" />
				<Router onChange={this.handleRoute} hashHistory={true}>
					<Home path="/" title="Belvedere Vodka" />
					<Customize path="/customize-e-compre" title="Belvedere Vodka - Customize e compre" />
					<BeInspired path="/inspire-se/" title="Belvedere Vodka - Inpire-se" />
					<Contact path="/fale-conosco/" title="Belvedere Vodka - Fale conosco" />
					<YourBelvedere path="/sua-belvedere" title="Belvedere Vodka - Sua belvedere" />
					<Checkout path="/checkout" title="Belvedere Vodka - Checkout"/>
					<MyProducts path="/meus-pedidos" title="Belvedere Vodka - Meus pedidos"/>
				</Router>
				<PageModal />
			</div>
		);
	}
}
