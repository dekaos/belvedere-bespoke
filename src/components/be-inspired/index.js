import { h, Component } from 'preact';
import style from './style.scss';
import Sidebar from '../sidebar';
import BelvedereBespoke from '../belvedere-bespoke';
import Youtube from 'react-youtube';
import { Helmet } from "react-helmet";
import Header from '../header';
import Swiper from './vendor/js/swiper.min.js';
import logo from '../../assets/images/logo-vodka.svg';
import rightArrow from '../../assets/images/right-arrow.svg';
import leftArrow from '../../assets/images/left-arrow.svg';
import beInspiredLargeImageOne from '../../assets/images/be-inspired-large-1.jpg';
import beInspiredLargeImageTwo from '../../assets/images/be-inspired-large-2.jpg';
import beInspiredLargeImageTree from '../../assets/images/be-inspired-large-3.jpg';

export default class BeInspired extends Component {
	state = {
		isVideoPlaying: false,
		showGallery: false
	};

	constructor () {
		super();
		this.player = false;
		this.loaded = false;
		this.swiper = null;
	}

	// gets called just before navigating away from the route
	componentWillUnmount() {}

	componentDidMount () {
		this.initSwiper();
	}

	initSwiper () {
		const img1 = new Image();
		const img2 = new Image();
		const img3 = new Image();

		img1.src = beInspiredLargeImageOne;
		img2.src = beInspiredLargeImageTwo;
		img3.src = beInspiredLargeImageTree;

		this.swiper = new Swiper ('.swiper-container', {
			loop: true,
			slidesPerView: 1,
			speed: 600,
			grabCursor: true,
			effect: 'slide',
			pagination: {
				el: '.swiper-pagination'
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			}
		});

		return this.swiper;
	}

	showGallery (e) {
		e.preventDefault();
		this.setState({showGallery: true});

		setTimeout(() => {
			this.swiper.destroy(true, true);
			this.initSwiper();
		});
	}

	closeGallery (e) {
		e.preventDefault();
		this.setState({showGallery: false});
	}

	render(_, {isVideoPlaying}) {
		const playerOptions = {
			playerVars: {
				controls: 0,
				ref: 0,
				showinfo: 0,
				autoplay: 0
			}
		};

		const onVideoReady = (event) => {
			this.player = event.target;
		};

		const playVideo = () => {
			this.player.playVideo();
			setTimeout(() => {
				this.setState({isVideoPlaying: true});
			}, 150);
		};

		return (
			<div class={`${style.beInspired} wrap`}>
				<Header />
				<Helmet title="Belvedere Vodka - Inspire-se" />
				<div class="inner">
					<div class="container flex">
						<div class={`${style.mainContent} animated fadeIn flex`}>
							<div class={style.leftContent}>
								<h1>Inspire-se</h1>
								<small>
									Aqui estão algumas ideias de customização para você criar sua própria identidade na garrafa ou para presentear alguém especial de forma única.
								</small>
								<blockquote>
									“Lembre-se que o nome de uma pessoa é para essa pessoa o som mais doce e mais importante em qualquer idioma”
								</blockquote>
								<cite>
									- Dale Carnegie
								</cite>
								<div class={style.largeScreen}>
									<BelvedereBespoke />
								</div>
							</div>
							<div class={style.rightContent}>
								<div class={style.player}>
									<div class={`${style.playerInner} ${isVideoPlaying ? style.isPlaying : ''}`}>
										<Youtube videoId="WggfK19km9A" onReady={onVideoReady} opts={playerOptions} />
										<button class={style.playVideo} onClick={function () {playVideo();}}>
											<span />
										</button>
									</div>
								</div>
								<div class={style.thumbs}>
									<a href="#" onClick={(e) => {this.showGallery(e);}}/>
									<a href="#" onClick={(e) => {this.showGallery(e);}} />
									<a href="#" onClick={(e) => {this.showGallery(e);}} />
								</div>
								<div class={style.lowScreen}>
									<BelvedereBespoke />
								</div>
							</div>
						</div>
						<Sidebar />
					</div>
				</div>
				<div class={`${style.galleryWrapper} ${this.state.showGallery === true ? style.visible : ''}`}>
					<header  class={`${style.header}`}>
						<div class="container flex">
							<a class={style.logo} href="#" onClick={(e) => {this.closeGallery(e);}}><img src={logo} /></a>
						</div>
					</header>
					<a href="#" class={style.closeGallery} onClick={(e) => {this.closeGallery(e);}} />
					<div class="left-tree" />
					<div class="right-tree" />
					<div class={`${style.galleryOuter} container`}>
						<div class={`${style.galleryInner} swiper-container`}>
							<div class={`${style.swiperWrapper} swiper-wrapper`}>
								<div class={`${style.swiperSlide} swiper-slide`}><div class={style.swiperImage}><div class={style.largeImageOne} /></div></div>
								<div class={`${style.swiperSlide} swiper-slide`}><div class={style.swiperImage}><div class={style.largeImageTwo} /></div></div>
								<div class={`${style.swiperSlide} swiper-slide`}><div class={style.swiperImage}><div class={style.largeImageTree} /></div></div>
							</div>
							<div class="swiper-pagination"/>
							<div class={`${style.navigationLeft} swiper-button-prev`}><img src={leftArrow}/></div>
							<div class={`${style.navigationRight} swiper-button-next`}><img src={rightArrow}/></div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
