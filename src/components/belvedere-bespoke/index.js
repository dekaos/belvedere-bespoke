import { h } from 'preact';
import style from './style.scss';
import SocialIcons from '../social-icons';

const BelvedereBespoke = () => (
	<div class={style.belvedereBespoke}>
		<div class="container">
			<h4><span>#Belvedere</span>Bespoke</h4>
			<SocialIcons />
		</div>
	</div>
);

export default BelvedereBespoke;
