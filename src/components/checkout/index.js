import { h, Component } from 'preact';
import style from './style.scss';
import Header from '../header';
import Footer from '../footer';
import arrowUp from '../../assets/images/arrow-up.svg';
import arrowDown from '../../assets/images/arrow-down.svg';
import productImg from '../../assets/images/product.jpg';
import info from '../../assets/images/info.svg';
import closeBlue from '../../assets/images/close-blue.svg';
import famousDrink from '../../assets/images/famous-drink.svg';
import store from 'store';
import map from 'lodash/map';
import findindex from 'lodash/findIndex';
import minus from '../../assets/images/minus.svg';
import plus from '../../assets/images/plus.svg';
import queryString from 'query-string';
import sanitize from '../sanitize';
import DaysOfOperationModal from '../days-of-operation-modal';

class Checkout extends  Component {

	constructor () {
		super();
		this.query = queryString.parse(location.hash);
	}

	state = {
		quantity: null,
		products: this.getStoredProducts(),
		total: null
	};

	productMatch (productId) {
		return findindex(this.getStoredProducts(), (product => product.id === productId));
	}

	updateQuantity (e, productId, action) {
		e.preventDefault();
		if (!this.productMatch(productId) && this.productMatch(productId) !== 0) {
			return;
		}

		const element = document.querySelector(`#input-${productId}`);

		if (element) {
			const products = this.getStoredProducts();

			if (action === 'increase') {
				element.value = Number(element.value) + 1;
			} else if (action === 'decrease') {
				if (element.value > 1) {
					element.value = Number(element.value) - 1;
				}
			}

			if (this.query.id === productId) {
				this.setState({...this.state, quantity: element.value});
			}

			products[this.productMatch(productId)].quantity = element.value;
			products[this.productMatch(productId)].subtotal = 649.99 * element.value;
			this.updateProducts(products);
		}
	}

	updateProducts (products) {
		store.set('products', products);

		setTimeout(() => {
			this.setState({...this.state, products});
			this.setTotal();
		});
	}

	removeProduct (e, productId) {
		e.preventDefault();
		const products = this.getStoredProducts().filter(product => product.id !== productId);
		this.updateProducts(products);
	}

	getStoredProducts () {
		return store.get('products') || [];
	}

	setTotal () {
		const products = this.state.products;
		if (products.length > 0) {
			let total = this.getStoredProducts().reduce((item, subtotal) => item + subtotal['subtotal'], 0);
			total = total.toFixed(2).replace('.', ',');
			this.setState({...this.state, total});
		} else {
			this.setState({...this.state, total: '0,00'});
		}
	}

	componentDidMount() {
		this.setState({quantity: Number(this.query.quantity)});
		this.setTotal();
	}

	currentProduct () {
		return this.query.id && this.query.message && this.query.quantity;
	}

	render (_, {quantity, products, total}) {

		return (
			<div class={`${style.checkout} wrap`}>
				<DaysOfOperationModal/>
				<Header hideMenu={true} />
				<div class="inner">
					<div class="container">
						<h1>Meus pedidos</h1>
						<form action="https://www.bebidasfamosas.com.br/belvederebespoke/session/create" method="post">
							<ul class={style.productList}>
								<li class={style.productListHeader}>
									<b>Produto</b>
									<b>Descrição</b>
									<b>Mensagem</b>
									<b>Valor unitário</b>
									<b>Quantidade</b>
									<b>Subtotal</b>
								</li>
								{
									map(products, (product, index) => (
										<li key={product.id} class={style.product}>
											<div class={`${style.productImg} ${style.productInner}`}>
												<b class={style.title}>Produto</b>
												<img src={productImg} />
											</div>
											<div class={`${style.productDescription} ${style.productInner}`}>
												<b class={style.title}>Descrição</b>
												<span>
													Belvedere Bespoke 1,75L Personalizada
												</span>
											</div>
											<div class={`${style.productMessage} ${style.productInner}`}>
												<b class={style.title}>Mensagem</b>
												<span>
													{ sanitize(product.message) }
													<button onClick={(e) => {e.preventDefault();}}>
														<img src={info} />
													</button>
												</span>
											</div>
											<div class={`${style.productInner} ${style.unitPrice}`}>
												<b class={style.title}>Valor unitário</b>
												<span>
													R$ 649.99
												</span>
											</div>
											<div class={`${style.quantityBox} ${style.productInner}`}>
												<b class={style.title}>Quantidade</b>
												<div class={style.quantityBoxInner}>
													<input type="number" id={`input-${product.id}`} min="1" value={product.quantity} onKeyUp={(e) => { this.updateQuantity(e, product.id, false); }}/>
													<button class={style.arrowUp} onClick={(e) => { this.updateQuantity(e, product.id, 'increase'); }}>
														<img src={arrowUp} />
													</button>
													<button class={style.arrowDown} onClick={(e) => { this.updateQuantity(e, product.id, 'decrease'); }}>
														<img src={arrowDown} />
													</button>
													<button class={`${style.arrowDown} ${style.mb}`} onClick={(e) => { this.updateQuantity(e, product.id, 'decrease'); }}>
														<img src={minus} />
													</button>
													<button class={`${style.arrowUp} ${style.mb}`} onClick={(e) => { this.updateQuantity(e, product.id, 'increase'); }}>
														<img src={plus} />
													</button>
												</div>
											</div>
											<div class={`${style.subtotal} ${style.productInner}`}>
												<b class={style.title}>Subtotal</b>
												<span>R$ {product.subtotal.toFixed(2).replace('.', ',')}</span>
												<button onClick={(e) => { this.removeProduct(e, product.id); }}>
													<img src={closeBlue} />
												</button>
											</div>
											<input type="hidden" name={`products[${index}][text]`} value={sanitize(product.message.toUpperCase())} />
											<input type="hidden" name={`products[${index}][qty]`} value={product.quantity} />
										</li>
									))
								}
							</ul>
							<div class={style.productsFooter}>
								<div class={style.addNewProduct}>
									<a href="/customize-e-compre" class={style.addNewLink}>
										<span>Adicionar nova garrafa</span>
									</a>
									<span class={style.productTotal}>
										<span>Total: R$ {total} + </span> Frete
									</span>
									<p>
										<b>Tem um promocode?</b> Você poderá usar ele na próxima etapa e garantir seu desconto
									</p>
									{this.currentProduct() ? (<a href={`/customize-e-compre/#id=${this.query.id}&message=${encodeURIComponent(this.query.message)}&quantity=${quantity}`} class={style.goBack}>Voltar e editar</a>) : ''}
								</div>
								<div class={style.productFinish}>
									<span class={style.productTotal}>
										<span>Total: R$ {total} + </span> Frete
									</span>
									<button type="submit" onClick={() => {this.setState({loading: true});}} class={style.finishBuy}>
										<span>
											Finalizar pedido
										</span>
										<div class={`${style.loading} ${this.state.loading === true ? style.visible : ''}`} />
									</button>
									<p>
										Ao clicar em <b>finalizar o pedido</b> você será redirecionado para o nosso distribuidor:
									</p>
									<img src={famousDrink} />
									{this.currentProduct() ? (<a href={`/customize-e-compre/#id=${this.query.id}&message=${encodeURIComponent(this.query.message)}&quantity=${quantity}`} class={style.goBack}>Voltar e editar</a>) : ''}
								</div>
							</div>
						</form>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}

export default Checkout;
