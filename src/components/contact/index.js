import { h, Component } from 'preact';
import style from './style.scss';
import Sidebar from '../sidebar';
import BelvedereBespoke from '../belvedere-bespoke';
import { Helmet } from "react-helmet";
import Header from '../header';
import Footer from '../footer';

export default class Contact extends Component {

	// gets called when this route is navigated to
	componentDidMount() {}

	// gets called just before navigating away from the route
	componentWillUnmount() {}

	render() {
		return (
			<div class={`${style.contact} wrap`}>
				<Header />
				<Helmet title="Belvedere Vodka - Fale Conosco" />
				<div class="inner">
					<div class="container flex">
						<div class={`${style.mainContent} animated fadeIn`}>
							<h1>Fale Conosco</h1>
							<p>Para um atendimento personalizado:</p>
							<div class={`${style.phoneContent} flex`}>
								<span class={style.phoneIcon} />
								<strong>
									Telefone: (11) 2613-1445<br />
									De Segunda a Sexta-Feira, das 08:00 às 18:00
								</strong>
							</div>
							<div class={`${style.mailContent} flex`}>
								<span class={style.mailIcon} />
								<strong>E-mail: belvederebespoke@bebidasfamosas.com.br</strong>
							</div>
							<div class={style.distribution}>
								<h4>Distribuição:</h4>
								<div class={style.drinkLogo} />
							</div>
						</div>
						<Sidebar />
					</div>
				</div>
				<div className={style.shareIcons}>
					<BelvedereBespoke />
				</div>
				<Footer/>
			</div>
		);
	}
}
