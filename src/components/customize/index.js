import { h, Component } from 'preact';
import style from './style.scss';
import { Helmet } from "react-helmet";
import Header from '../header';
import { route } from 'preact-router';
import shortid from 'shortid';
import store from 'store';
import { badWords } from './bad-words';
import queryString from 'query-string';
import hideVirtualKeyboard from 'hide-virtual-keyboard';
import some from 'lodash/some';
import sanitize from '../sanitize';

export default class Customize extends Component {

	constructor () {
		super();
		this.player = null;
		this.products = store.get('products') || [];
		this.timer = null;
		this.hasHistory = typeof history !== 'undefined';
		this.query = queryString.parse(location.hash);
	}

	state = {
		message: null,
		showVideo: false,
		badWord: false,
		bigWord: false
	};

	lockOrientation () {
		if (navigator.userAgent.match( /(android|iphone)/gi)) {
			let locOrientation = screen.lockOrientation || screen.mozLockOrientation || screen.msLockOrientation;

			if (locOrientation) {
				locOrientation('portrait');
			} else if (screen.orientation) {
				screen.orientation.lock('portrait');
			}
		}
	}

	// gets called when this route is navigated to
	componentDidMount() {

		setTimeout(() => {
			window.scrollTo(0, 200);
			if (this.query.message) this.setState({...this.state, message: this.query.message.toLowerCase(), wordCount: 13 - this.query.message.length});
		});

		const bottleTextInput = this.bottleText;
		bottleTextInput.focus();

		this.bottleInner.addEventListener('click',  () => {
			return bottleTextInput.focus();
		});

		this.video.muted = true;
		this.video.preload = true;

		this.video.load();
	}

	addMessage (e) {
		let message = e.target.value;
		let counter;

		if (message.length <= 13) {
			counter = 13 - e.target.value.length;
		}

		message = sanitize(message);

		if (message.length > 13) {
			message = message.substr(0, 13);
			counter = 0;
		}

		this.setState({message});

		if (e.which !== 13) {
			this.setState({...this.state, badWord: false, wordCount: counter});
		}

		if (this.hasHistory) {
			window.history.replaceState( {} , 'customize-e-compre', `/customize-e-compre/#message=${message}` );
		}
	}

	testWord(message) {
		let curseWord = false;
		const testWord = message.split(' ');

		some(testWord, (word) => {
			curseWord = badWords.filter((badWord) => { return badWord === word; });

			if (curseWord.length > 0) {
				curseWord = true;
				return true;
			}
		});

		if (testWord[0] === 'grey' && testWord[1] && testWord[1] === 'goose') curseWord = true;

		return curseWord;
	}

	getPathName() {
		return location.pathname;
	}

	goToCustomizedBottle(id, message, quantity, timer) {
		this.setState({showVideo: false});

		if (timer) {
			clearTimeout(timer);
		}

		if (this.getPathName() === '/customize-e-compre/') {
			route(`/sua-belvedere/#id=${id}&message=${encodeURIComponent(message)}&quantity=${quantity}`);
		}
	}

	showCustomBottle (e) {
		e.preventDefault();
		let timer;
		let message = this.state.message;
		const quantity = this.query.quantity || 1;
		const id = this.query.id || shortid.generate().toLowerCase();

		message = message.toLowerCase().trim();

		if (this.testWord(message) === true) {

			this.setState({...this.state, badWord: true});

			setTimeout(() => {
				this.setState({...this.state, badWord: false, message: null});
			}, 500);

			return;
		}

		hideVirtualKeyboard();

		this.setState({showVideo: true});

		if (this.video.requestFullscreen) {
			this.video.requestFullscreen();
			this.lockOrientation();
		} else if (this.video.msRequestFullscreen) {
			this.video.msRequestFullscreen();
			this.lockOrientation();
		} else if (this.video.mozRequestFullScreen) {
			this.video.mozRequestFullScreen();
			this.lockOrientation();
		} else if (this.video.webkitRequestFullscreen) {
			this.video.webkitRequestFullscreen();
			this.lockOrientation();
		}

		this.video.play();

		this.video.addEventListener('ended', () => {
			this.goToCustomizedBottle(id, message, quantity, timer);
		});

		this.video.addEventListener('pause', () => {
			this.goToCustomizedBottle(id, message, quantity, timer);
		});

		timer = setTimeout(() => {
			this.goToCustomizedBottle(id, message, quantity, null);
		}, 8000);
	}

	// gets called just before navigating away from the route
	componentWillUnmount() {
		this.video.removeEventListener('ended', () => {}, false);
		this.video.removeEventListener('pause', () => {}, false);
	}

	render(_, {message, showVideo}) {

		return (
			<div class={`${style.customize} wrap`}>
				<Header />
				<Helmet title="Belvedere Vodka - Customize e Compre" />
				<div class="inner">
					<div class="container animated fadeIn">
						<h1>Customize e compre sua Belvedere</h1>
						<form onSubmit={(e) => {this.showCustomBottle(e);}} class={`${this.state.badWord === true ? style.badWord : ''}`}>
							<div class={`${style.customBottle} ${this.state.bigWord === true ? style.reducedFontSize: ''}`}>
								<div class={style.customBottleFlare} />
								<div class={style.customBottleHorizontal}>
									<div class={style.customBottleInner} ref={(bottleInner) => {this.bottleInner = bottleInner;}}>
										<input class={style.bottleText} ref={(bottleText) => {this.bottleText = bottleText;}} maxLength="13" type="text" value={message} onKeyUp={(e) => {this.addMessage(e);}}/>
									</div>
								</div>
								<div class={style.customBottleHorizontalReflex} />
								<div class={style.addMessageOnBottle}>
									<small>{!this.state.wordCount && this.state.wordCount !== 0 ? 13 : this.state.wordCount} | Sua mensagem:</small>
									<div class={style.bottleTextArea}>
										<input type="text" placeholder="Escreva sua mensagem" class={style.secondInput} onKeyUp={(e) => {this.addMessage(e);}} maxLength="13" value={message} />
										<button type="submit" class={style.showCustomBottle} disabled={!message}><span>Visualizar garrafa</span></button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<footer>
					<div class="container">
						<p>
							Confira atentamente a grafia adicionada na garrafa. Após confirmado o pedido não haverá ressarcimento do produto em caso de erro de digitação.
							Qualquer mensagem discriminatória, difamatória, ou linguagem ofensiva não será aceita.
						</p>
					</div>
				</footer>
				<video ref={(video) => {this.video = video;}} preload class={showVideo === true ? style.showVideo: ''} muted controls>
					<source src="//videotest-skyisclear.rhcloud.com/belvedere-video-load-1.mp4" type="video/mp4"/>
					<source src="//videotest-skyisclear.rhcloud.com/belvedere-video-load-1.webm" type="video/webm"/>
				</video>
			</div>
		);
	}
}
