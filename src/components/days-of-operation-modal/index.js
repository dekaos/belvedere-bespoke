import {h, Component } from 'preact';
import moment from 'moment';
import store from 'store';
import style from './style.scss';

class DaysOfOperationModal extends Component {
	
	state = {
		closeModal: true,
		text: null
	};
	
	showModal () {
		if (moment().isAfter('2017-12-21') === true && moment().isBefore('2017-12-25 15:00')) {
			this.setState({...this.state, closeModal: false, text: 'Pedidos feitos a partir hoje serão processados somente na semana de 26 de dezembro.'});
			this.toggleBodyClass();
		} else if (moment().isAfter('2017-12-25 15:00:01') === true && moment().isBefore('2018-01-04')) {
			this.setState({...this.state, closeModal: false, text: 'Pedidos feitos a partir de hoje serão processados a partir de 4 de janeiro.'});
			this.toggleBodyClass();
		}
	}
	
	toggleBodyClass () {
		document.querySelector('body').classList.toggle('hideScroll');
	}
	
	userConfirmation() {
		return store.get('confirmedUser');
	}
	
	componentDidMount () {
		let interval;
		
		if (this.userConfirmation() === 'y') {
			this.showModal();
		} else {
			interval = setInterval(() => {
				if (this.userConfirmation() === 'y') {
					this.showModal();
					clearInterval(interval);
				}
			}, 2000);
		}
	}
	
	closeDaysOfOperationModal (e) {
		e.preventDefault();
		this.setState({...this.state, closeModal: true});
		this.toggleBodyClass();
	}
	
	componentWillUnmount () {
		document.querySelector('body').classList.remove('hideScroll');
	}
	
	render (_, {closeModal, text}) {
		return (
			<div class={`${style.daysOfOperationModalWrapper} ${closeModal === false ? style.visible : ''}`} onClick={(e) => {this.closeDaysOfOperationModal(e);}}>
				<div class={`${style.daysOfOperationModal} animated bounceInDown`} onClick={(e) => {e.stopPropagation(); e.stopImmediatePropagation();}}>
					<a class={style.closeDaysOfOperationModal} onClick={(e) => {this.closeDaysOfOperationModal(e);}} />
					<div>
						<h6>Expediente de natal e ano novo</h6>
						<p>
							{text}
						</p>
					</div>
				</div>
			</div>
		);
	}
}

export default DaysOfOperationModal;
