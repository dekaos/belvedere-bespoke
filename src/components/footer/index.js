import { h } from 'preact';
import style from './style.scss';

const Footer = () => (
	<footer class={style.footer}>
		<div class="container flex">
			<small class={style.copy}>
				Copyright 2017 © Belvedere Vodka. All rights reserved.
			</small>
		</div>
	</footer>
);

export default Footer;
