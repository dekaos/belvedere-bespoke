import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style.scss';
import logo from '../../assets/images/logo-vodka.svg';
import SocialIcons from '../social-icons';
import Footer from '../footer';
import closeWhite from '../../assets/images/close-white.svg';
import Cart from '../my-products/cart';

export default class Header extends Component {

	state = {
		dropMenuOpen: false
	};

	toggleDropMenu (e) {
		e.preventDefault();
		this.setState({dropMenuOpen: !this.state.dropMenuOpen});
	}

	render(_, {dropMenuOpen}) {
		return (
			<header id="headerMenu" class={`${style.header} ${this.props.hideMenu === true ? style.hideMenu : ''} ${dropMenuOpen === true ? style.dropMenuOpened : style.dropMenuClosed}`}>
				<div class="container flex center-vertical">
					<Link activeClassName={style.active} class={style.logo} href="/"><img src={logo} /></Link>
					<div class={style.horizontalMenu}>
						<nav>
							<Link activeClassName={style.active} class={style.navLink} href="/customize-e-compre">Customize e compre</Link>
							<Link activeClassName={style.active} class={style.navLink} href="/inspire-se">Inspire-se</Link>
							<Link activeClassName={style.active} class={style.navLink} href="/fale-conosco">Fale conosco</Link>
						</nav>
						<span class={style.headerCart}>
							<Cart myProductsPage={false} />
						</span>
					</div>
					<div class={style.dropMenu}>
						<a class={style.toggleDropMenu} href="#" onClick={(e) => {this.toggleDropMenu(e);}}>
							<span />
							<span />
							<img src={closeWhite} />
						</a>
						<div class={`${style.dropMenuNav}`}>
							<div class={style.dropMenuInner}>
								<nav>
									<Link activeClassName={style.active} class={style.navLink} href="/">Crie a sua</Link>
									<Link activeClassName={style.active} class={style.navLink} href="/inspire-se">Inspire-se</Link>
									<Link activeClassName={style.active} class={style.navLink} href="/fale-conosco">Fale conosco</Link>
									<Link activeClassName={style.active} class={style.navLink} href="/meus-pedidos">Meus pedidos</Link>
								</nav>
								<Link class={style.customizeAndBuy} href="/customize-e-compre">
									<span>Customize e compre</span>
								</Link>
								<small>
									Belvedere é uma opção de qualidade. Beba com estilo e não em excesso.
								</small>
								<SocialIcons />
							</div>
							<Footer />
						</div>
					</div>
				</div>
			</header>
		);
	}
}
