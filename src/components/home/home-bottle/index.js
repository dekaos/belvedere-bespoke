import { h, Component } from 'preact';
import style from './style.scss';

class HomeBottle extends Component {
	constructor () {
		super();
		this.interval = null;
	}

	state = {
		word: this.randomGenerator()
	}

	randomGenerator () {
		const words = [
			'Anderson',
			'Nathan',
			'Bruno',
			'Gisele',
			'Tatiane',
			'Carol',
			'Thiago',
			'Estevão',
			'João',
			'Henrique'
		];

		return words[Math.floor(Math.random() * words.length)];
	}

	componentDidMount () {
		this.interval = setInterval(() => {
			this.randomWords.innerHTML = `<p class="animated fadeIn">${this.randomGenerator()}</p>`;
		}, 6000);
	}

	componentWillUnmount () {
		clearInterval(this.interval);
	}

	render (_, {word}) {
		return (
			<div class={style.homeBottle}>
				<div class="container flex">
					<div class={style.homeBottleInner}>
						<div class={style.leftBaseBottle} />
						<div class={style.rightBaseBottle} />
						<div class={style.leftLedBottle} />
						<div class={style.rightLedBottle}>
							<div class={style.rightBottleInner} />
							<div ref={(randomWords) => this.randomWords = randomWords} class={style.randomWords}>
								<p class="animated fadeIn">{word}</p>
							</div>
						</div>
						<div class={style.lensFlare} />
					</div>
				</div>
			</div>
		);
	}
}

export default HomeBottle;
