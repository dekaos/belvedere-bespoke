import { h, Component } from 'preact';
import { Helmet } from "react-helmet";
import style from './style.scss';
import Sidebar from '../sidebar';
import { Link } from 'preact-router/match';
import SocialIcons from '../social-icons';
import Footer from '../footer';
import HomeBottle from './home-bottle';
import Header from '../header';

export default class Home extends Component {

	render() {
		return (
			<div class={`${style.home} wrap`}>
				<Header />
				<Helmet title="Belvedere Vodka" />
				<div class="inner">
					<div class="container flex">
						<div class={`${style.mainContent} animated fadeIn`}>
							<h1>Crie a sua</h1>
							<h2><span>#Belvedere</span>Bespoke</h2>
							<div class={style.smallBottle}>
								<HomeBottle />
							</div>
							<p>
							Agora você pode personalizar sua própria edição limitada da <strong>Belvedere Vodka</strong>. O presente perfeito para festividades, casamentos e aniversários. Esta coleção feita sob medida tornará sua ocasião tão única como a própria garrafa iluminada com LED.
							</p>
							<Link class={style.customizeAndBuy} href="/customize-e-compre">
								<span>Customize e compre</span>
							</Link>
							<small>
							Belvedere é uma opção de qualidade. Beba com estilo e não em excesso.
							</small>
							<SocialIcons />
						</div>
						<Sidebar />
					</div>
				</div>
				<div class={style.largeBottle}>
					<HomeBottle />
				</div>
				<Footer />
			</div>
		);
	}
}
