import { h, Component } from 'preact';
import map from 'lodash/map';
import productImg from '../../../assets/images/product.jpg';
import store from 'store';
import style from './style.scss';
import sanitize from '../../sanitize';

class Cart extends Component {
	state = {
		openCart: false,
		products: [],
		total: '0,00',
		productsQtd: 0,
		productsQuantity: 0,
		dropMenuOpen: false
	};

	openCart (e) {
		e.preventDefault();
		this.setState({...this.state, openCart: !this.state.openCart});
	}

	componentDidMount () {
		const products = store.get('products');
		if (products) {
			let total = products.reduce((item, subtotal) => item + subtotal['subtotal'], 0);
			total = total.toFixed(2).replace('.', ',');
			this.setState({...this.state, products, total, productsQuantity: products.length});
		}
	}

	render (_, {openCart, products, total, productsQuantity}) {
		return (
			<div class={`${style.cart} ${openCart === true ? style.visible : ''} ${this.props.myProductsPage === true ? style.myProductsPage : ''}`}>
				<a class={`${style.cartLink} flex center-vertical`} href="#" onClick={(e) => {this.openCart(e);}}><span class={style.cartIcon} /><span>Meus pedidos {productsQuantity > 0 ? (<span class={style.quantity}>({productsQuantity})</span>) : ''}</span></a>
				<div class={style.cartList}>
					<header>
						<b>Pedidos</b>
					</header>
					{products.length ? (
						<div>
							<ul>
								{
									map(products, (product) => (
										<li key={product.id}>
											<a href="/checkout/">
												<div class={style.productImg}>
													<img src={productImg} />
												</div>
												<div class={style.productDescription}>
													<small>Belvedere Bespoke 1,75L Personalizada</small>
													<b>{sanitize(product.message)}</b>
												</div>
											</a>
										</li>
									))
								}
							</ul>
							<div class={style.total}>
								<b>Total:</b> R$ {total} + Frete
							</div>
						</div>

					) : (
						<div class={style.emptyCart}>
							<p>
								Não há pedidos adicionados ao carrinho.
							</p>
						</div>
					)}
				</div>
			</div>
		);
	}
}


export default Cart;
