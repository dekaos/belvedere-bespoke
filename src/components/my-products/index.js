import { h } from 'preact';
import Header from '../header';
import style from './style.scss';
import Cart from './cart';

const MyProducts = () => {
	return (
		<div class={`${style.myProducts} wrap`}>
			<Header/>
			<div className="container">
				<Cart myProductsPage={true} />
			</div>
		</div>
	);
};

export default MyProducts;
