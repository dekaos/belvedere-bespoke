import { h, Component } from 'preact';
import moment from 'moment';
import store from 'store';
import style from './style.scss';
import VMasker from 'vanilla-masker';

class PageModal extends Component {

	state = {
		confirmedUser: true,
		formError: false,
		errorMessage: null,
		isAdult: true,
		page: 'one'
	};

	componentDidMount() {

		const confirmedUser = store.get('confirmedUser');
		if (!confirmedUser) {
			this.setState({confirmedUser: false});
			VMasker(document.querySelector(".age")).maskPattern("99/99/9999");
		}
	}

	validaForm (e) {
		moment.locale('pt-BR');

		e.preventDefault();

		const inputDate = moment(document.querySelector('.age').value, "DD/MM/YYYY", true);

		if (!inputDate.isValid()) {
			this.setState({formError: true, errorMessage: 'Insira a data do seu aniversário.'});
			return;
		}

		const years = moment().diff(moment(inputDate), 'years', false);

		if (years >= 18) {
			store.set('confirmedUser', 'y');
			this.setState({confirmedUser: true});
		} else {
			this.setState({formError: true, errorMessage: 'Conteúdo para maiores de 18 anos.'});
		}
	}

	onChange () {
		this.setState({formError: false});
	}

	showTerms () {
		this.setState({page: 'two'});
	}

	render (_, { confirmedUser, formError, errorMessage, page }) {
		return (
			<div class={`${style.modalWrapper} ${!confirmedUser ? style.visible : ''}`}>
				<div class={`${style.modalOuter} animated bounceInDown ${page === 'one' ? style.modalContentOne : style.modalContentTwo}`}>
					<div class={style.modalWhiteShadow} />
					<div class={style.modalInner}>
						<div class={style.modalContent}>
							<div class={style.modalInnerContent}>
								<div class={style.modalAgeConfirmation}>
									<div class={style.modalLogo} />
									<form onSubmit={(e) => {this.validaForm(e);}} class={formError ? style.error : ''}>
										<fieldset>
											<legend>Por favor insira a data do seu aniversário</legend>
											<div class={style.modalAgeInput}>
												<input name="age" class="age" type="tel" placeholder="DD/MM/AAAA" onChange={() => {this.onChange();}} />
											</div>
										</fieldset>
										<input type="submit" value="Entrar" />
										<div class={style.errorMsg}>
											<small>{ errorMessage }</small>
										</div>
									</form>
									<h4>#BELVEDEREBESPOKE</h4>
									<p>
										Para visitar <a href="http://www.belvederebespoke.com.br" target="_blank">www.belvederebespoke.com.br</a>, você deve ter a idade legal para beber no seu país de residência. Se não existir uma lei com uma idade mínima em seu país de residência, você deve ter mais que 18 anos de idade para visitar nosso site. Clicando em "Entrar" você concorda com os nossos termos e condições de uso, encontrados <a href="#" onClick={() => { this.showTerms(); }}>aqui</a>
									</p>
									<p>
										O abuso de bebidas alcoólicas é prejudicial a saúde.
									</p>
									<p>
										Belvedere é uma opção de qualidade e apoia o consumo responsável de seus produtos através da Moët Hennessy, membro SpiritsEUROPE (<a href="//www.responsibledrinking.eu" target="_blank">www.responsibledrinking.eu</a>), DISCUS (<a href="//www.discus.org" target="_blank">www.discus.org</a>), CEEV (<a href="//www.wineinmoderation.eu" target="_blank">www.wineinmoderation.eu</a> ) e Entreprise & Prévention (<a href="//www.soifdevivre.com" target="_blank">www.soifdevivre.com</a>).
									</p>
								</div>
								<div class={style.modalTermsPage}>
									<h4>
										Termos e condições de Uso e Política de privacidade
									</h4>
									<p>
										O conteúdo deste site é operado pela Moët Hennessy do Brasil estabelecida na Avenida Brasil, 1.814, CEP 01430-001, São Paulo - SP ("Moët").
									</p>
									<p>
										Qualquer uso deste site por qualquer pessoa ("você" ou "usuário") é regido por estes termos e condições de uso ("termos e condições"). A finalidade destes termos e condições é definir as condições de acesso, navegação e uso do ambiente, além dos termos e condições de utilização da plataforma oferecida pelo site. Ao acessar e utilizar este site, você concorda, sem limites, com estes termos e condições.
									</p>
									<p>
										A Moët pode modificar ou atualizar estes termos e condições e a política de dados pessoais a qualquer momento e sem aviso prévio. É sua responsabilidade ler os termos e condições novamente.
									</p>
									<p>
										A Moët oferece esse site apenas para as pessoas que tenham atingido a idade legal para consumir ou comprar bebidas alcoólicas em seu país de residência e desde que o consumo ou a venda de bebidas alcoólicas seja legal no seu país. Se não existirem restrições no seu país de residência, você deve ter mais de 21 anos para acessar este site.
									</p>
									<p>
										Este meio não é projetado para ser visto em países com restrições à publicidade de bebidas alcoólicas. A Moët apoia o consumo responsável e é membro da SpiritsEUROPE (<a href="//www.responsibledrinking.eu" target="_blank">www.responsibledrinking.eu</a>), DISCUS (<a href="//www.discus.org" target="_blank">www.discus.org</a>), CEEV (<a href="//www.wineinmoderation.eu" target="_blank">www.wineinmoderation.eu</a>) e Entreprise & Prévention (<a href="//www.soifdevivre.com" target="_blank">www.soifdevivre.com</a>).
									</p>
									<strong>
										Acesso ao Site
									</strong>
									<p>
										O hardware e software necessários para acessar a Internet e site são de exclusiva responsabilidade dos usuários.
									</p>
									<p>
										A Moët se reserva o direito de suspender ou cancelar o acesso ou navegação do site, seus conteúdos ou serviços disponíveis sem prévio aviso.
									</p>
									<strong>
										Links
									</strong>
									<p>
										Este site contém links para outros websites e recursos da Internet. A Moët não é responsável pela disponibilidade de sites ou recursos externos ligados por este Site através de link, e não apoia nem é responsável por quaisquer conteúdos, anúncios, produtos ou outros materiais contidos em ou disponibilizados através destes sites ou recursos. As transações que ocorrerem entre você e a terceira parte são estritamente entre você e esta terceira parte e não são de responsabilidade da Moët. Como a Moët não é responsável pela disponibilidade destes recursos externos ou por seus conteúdos, você deve consultar os Termos e Condições de Uso destes sites, já que suas políticas podem ser diferentes das nossas.
									</p>
									<strong>Comportamento Responsável do Usuário<br /></strong><strong>O usuário garante:</strong>
									<p>
										Ter a idade mínima para consumir ou comprar bebidas alcoólicas, em conformidade com os regulamentos do seu país de residência.
										Utilizar somente para seu uso pessoal e legítimo, portanto, excluindo qualquer outro uso, comercial ou não, sem o consentimento prévio por escrito da Moët.
									</p>

									<strong>O usuário deve abster-se especialmente de:</strong>
									<p>
										Transmitir ou enviar por correio eletrônico ou por quaisquer outros meios (incluindo o compartilhamento) qualquer conteúdo do site a pessoas que não tenham idade legal para beber em seu país de compra ou no seu país de residência ou residir em um país que restringe ou proíbe a publicidade e o consumo de bebidas alcoólicas.
									</p>
									<p>
										Interromper o site, seus servidores ou redes que se conectem ao site.
									</p>
									<p>
										Realizar qualquer atividade ilegal ou que possa ameaçar os direitos da Moët, suas subsidiárias, afiliadas, fornecedores (incluindo empreiteiros e fornecedores técnicos), usuários e terceiros.
									</p>
									<p>
										A Moët se reserva o direito, a seu exclusivo critério, de suspender ou retirar o acesso de qualquer usuário que demonstre comportamento persistentemente contrário aos Termos e Condições de Uso que regem o uso deste site
									</p>
									<strong>
										Proteção do Conteúdo do Site
									</strong>
									<p>
										O site e cada um dos seus elementos (incluindo todos os textos, imagens, som, vídeo, desenhos, marcas, logotipos e outros materiais em ou acessível no meio), bem como o software necessário para o uso em relação ao meio ambiente (o "conteúdo"), pode conter informações confidenciais e informações que estão protegidas por direitos de propriedade intelectual ou outras leis. A menos que expressamente indicado no site, a Moët detém todos os direitos de propriedade intelectual relacionados com o conteúdo e não concede a nenhum usuário qualquer licença ou qualquer direito relacionado com o conteúdo e as com imagens divulgados neste site.
									</p>
									<p>
										A reprodução do conteúdo (no todo ou em parte) do site está autorizada para o único propósito de obter informações para uso pessoal e privado. Qualquer reprodução ou utilização ou outras formas de exploração do conteúdo para qualquer outra finalidade (qualquer que seja a forma ou tipo) é expressamente proibida sem permissão prévia e por escrito da Moët.
									</p>
									<strong>ÚLTIMA ATUALIZAÇÃO: OUTUBRO DE 2017.</strong>
								</div>
							</div>
							<a class={style.goToModalContentOne} href="#" onClick={(e) => { e.preventDefault(); this.setState({page: 'one'});}}>Voltar ao início</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default PageModal;
