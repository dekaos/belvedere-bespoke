import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style.scss';

const Sidebar = () => (
	<div class={style.sidebar}>
		<nav class={style.sidebarNavigation}>
			<Link activeClassName={style.active} href="/">
				<span class={style.textLabel}>Crie a sua</span><span class={style.numberLabel}>01</span>
			</Link>
			<Link activeClassName={style.active} href="/inspire-se">
				<span class={style.textLabel}>Inspire-se</span><span class={style.numberLabel}>02</span>
			</Link>
			<Link activeClassName={style.active} href="/fale-conosco">
				<span class={style.textLabel}>Fale conosco</span><span class={style.numberLabel}>03</span>
			</Link>
		</nav>
	</div>
);

export default Sidebar;