import { h } from 'preact';
import style from './style.scss';
import facebookIcon from '../../assets/images/facebook.svg';
import instagramIcon from '../../assets/images/instagram.svg';
import mailIcon from '../../assets/images/mail.svg';

const SocialIcons = () => (
	<div class={style.socialIcons}>
		<a class={style.fbIcon} href="https://www.facebook.com/BelvedereVodkaBrasil/" target="_blank"><img src={facebookIcon}/></a>
		<a class={style.inIcon} href="https://www.instagram.com/belvederebrasil/" target="_blank"><img src={instagramIcon}/></a>
		<a class={style.emIcon} href="mailto:belvederebespoke@bebidasfamosas.com.br"><img src={mailIcon}/></a>
	</div>
);

export default SocialIcons;
