import { h } from 'preact';
import style from './style.scss';

const FreightModal = props => {

	return (
		<div id="freightModal" class={`${style.freightModalWrapper} ${props.show === true ? style.visible : ''}`}>
			<div class={`${style.freightModalOuter} animated bounceInDown`} onClick={(e) => {e.stopPropagation(); e.stopImmediatePropagation();}}>
				<a class={style.closeFreightModal} onClick={(e) => {props.toggleFreightModal(e);}} />
				<div class={style.freightModalInner}>
					<h4>Prazo de entrega</h4>
					<p>
						Por se tratar de um produto personalizado especialmente para você, o prazo de produção poderá sofrer variações.
					</p>
					<h6>Dúvidas?</h6>
					<div class={style.phone}><span class={style.phoneIcon} /><span>Telefone para contato: <strong>(11) 2613-1445</strong></span></div>
					<h6>Cálculo do Frete</h6>
					<div class={style.freight}>
						<span class={style.freightIcon} />
						<p>
							Ao clicar em <strong>“Finalizar o pedido”</strong> você será redirecionado ao site Bebidas Famosas para finalizar o seu pedido e cálculo do frete por região.
						</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default FreightModal;
