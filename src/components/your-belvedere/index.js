import { h, Component } from 'preact';
import Header from '../header';
import style from './style.scss';
import FreightModal from './freight-modal';
import arrowUp from '../../assets/images/arrow-up.svg';
import arrowDown from '../../assets/images/arrow-down.svg';
import store from 'store';
import { route } from 'preact-router';
import findindex from 'lodash/findIndex';
import queryString from 'query-string';
import minus from '../../assets/images/minus.svg';
import plus from '../../assets/images/plus.svg';
import sanitize from '../sanitize';

class YourBelvedere extends Component {

	constructor () {
		super();
		this.products = store.get('products') || [];
		this.query = this.getQueryString();
	}

	state = {
		quantity: 1,
		showFreightModal: false,
		total: 649.99,
		image: ''
	};

	updateTotal () {
		this.setState({total: 649.99 * this.state.quantity});
	}

	getQueryString() {
		return queryString.parse(location.hash);
	}

	updateQuantity (e) {
		const quantity = e.target.value;
		if (quantity >= 1) {
			this.setState({quantity});
		} else {
			this.setState({quantity: 1});
		}

		this.updateTotal();
	}

	increaseQuantity () {
		this.setState({quantity: this.state.quantity + 1});
		this.updateTotal();
	}

	decreaseQuantity () {
		if (this.state.quantity === 1) return;
		this.setState({quantity: this.state.quantity - 1});
		this.updateTotal();
	}

	toggleFreightModal (e) {
		e.preventDefault();
		window.scrollTo(0, 0);
		this.setState({showFreightModal: !this.state.showFreightModal});

		setTimeout(() => {
			document.querySelector('#freightModal').addEventListener('click', (e) => {
				return this.toggleFreightModal(e);
			});
		}, 1500);
	}

	getQuantity () {
		return this.query.quantity;
	}

	componentDidMount () {
		window.scrollTo(0, 0);

		setTimeout(() => {
			this.makeBottle();
		});
	}

	makeBottle() {
		let quantity = this.getQuantity();

		if (quantity) {
			this.setState({...this.state, quantity: Number(quantity)});
		}

		if (this.query.message) {
			const message = sanitize(this.query.message.toUpperCase());
			const canvas = document.createElement('canvas');
			document.querySelector('.wrap').appendChild(canvas);
			const context = canvas.getContext('2d');
			canvas.width = '73';
			canvas.height = this.messageHolder.offsetWidth;
			context.translate(canvas.width / 2, canvas.height / 2);
			context.rotate(-Math.PI / 2);
			context.fillStyle = 'rgba(255, 255, 255, .75)';
			context.font = '45pt Belvedere-Regular';
			context.shadowColor = 'rgba(255, 255, 255, .75)';
			context.shadowOffsetX = 2;
			context.shadowOffsetY = 3;
			context.shadowBlur = 6;
			context.textAlign = 'center';
			context.fillText(message, 0, 24);
			this.setState({image: context.canvas.toDataURL()});
		}
	}

	componentWillUpdate() {
		this.query = this.getQueryString();
	}

	addToCart (e) {
		e.preventDefault();
		const message = this.query.message;
		const id = this.query.id;
		const quantity = this.state.quantity;
		const productMatch = findindex(this.products, (product => product.id === this.query.id));

		if (this.products[productMatch]) {
			this.products[productMatch].message = message;
			this.products[productMatch].quantity = this.state.quantity;
			this.products[productMatch].subtotal = 649.99 * this.state.quantity;
		} else {
			this.products.push({id, message, quantity, subtotal: 649.99 * this.state.quantity});
		}

		store.set('products', this.products);

		setTimeout(() => {
			route(`/checkout/#id=${id}&message=${encodeURIComponent(message)}&quantity=${this.state.quantity}`);
		});
	}

	render (_, {quantity, showFreightModal, total}) {
		return (
			<div class={`${style.yourBelvedere} wrap`}>
				<Header hideMenu={true} />
				<div class="inner">
					<div class="container flex">
						<div class={style.leftContent}>
							<h1>Sua Belvedere Bespoke</h1>
							<div class={style.mBottle}>
								<div class={style.userBottle}>
									<div class={style.userBottleInner}>
										<div class={style.userBottleFlare} />
										<div class={style.userBottleImage}>
											<div class={style.userBottleMessage}>
												<div class={style.customBottleImage} style={`background: url(${this.state.image}) no-repeat center center`} />
											</div>
											<div class={style.userBottleLed} />
										</div>
										<div class={style.bottleCharge} />
									</div>
								</div>
							</div>
							<h2>Belvedere Bespoke 1,75L Personalizada<br />com Cartucho premium incluso</h2>
							<p class={style.unitValue}><b>VALOR:</b> R$ 649,99 a unidade.</p>
							<div class={style.quantityWrapper}>
								<label for="quantity">Quantidade:</label>
								<div class={style.quantityBox}>
									<input type="number" id="quantity" min="1" value={quantity} onChange={(e) => {this.updateQuantity(e);}}/>
									<button class={style.arrowUp} onClick={() => {this.increaseQuantity();}}>
										<img src={arrowUp} />
									</button>
									<button class={style.arrowDown} onClick={() => {this.decreaseQuantity();}}>
										<img src={arrowDown} />
									</button>
									<button class={`${style.arrowDown} ${style.mb}`} onClick={() => {this.decreaseQuantity();}}>
										<img src={minus} />
									</button>
									<button class={`${style.arrowUp} ${style.mb}`} onClick={() => {this.increaseQuantity();}}>
										<img src={plus} />
									</button>
								</div>
							</div>
							<div class={style.total}>
								<p>
									<b>TOTAL: R$ {total.toFixed(2).replace('.', ',')} + Frete</b> <a href="#" onClick={(e) => {this.toggleFreightModal(e);}}>(Consultar)</a>
								</p>
							</div>
							<a href="#" class={style.addToOrder} onClick={(e) => {this.addToCart(e);}}>
								<span>Adicionar ao pedido</span>
							</a>
							<a href={`/customize-e-compre/#id=${this.query.id}&message=${encodeURIComponent(this.query.message)}&quantity=${quantity}`} class={style.back}>Voltar e editar</a>
						</div>
					</div>
					<div class={style.largeBottle}>
						<div class={style.userBottle}>
							<div class="container">
								<div class={style.userBottleInner}>
									<div class={style.userBottleFlare} />
									<div class={style.userBottleImage}>
										<div class={style.userBottleMessage}>
											<div class={style.customBottleImage} style={`background: url(${this.state.image}) no-repeat center center`} />
										</div>
										<div class={style.userBottleLed} />
									</div>
									<div class={style.bottleCharge} />
								</div>
							</div>
							<footer />
						</div>
					</div>
					<span class={style.messageHolder} ref={(messageHolder) => this.messageHolder = messageHolder}>{this.query.message.toUpperCase()}</span>
				</div>
				<FreightModal toggleFreightModal={(e) => {this.toggleFreightModal(e);}} show={showFreightModal} />
			</div>
		);
	}
}

export default YourBelvedere;
